const express = require('express');
const app = express();



app.use(express.static('public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/accueil/index.html');
});

app.get('/api', function (req, res) {
    console.log(req.query);

    // 1 - allez chercher le fichier JSON


    var db = JSON.parse("[]"); 

    // 2 - Ajouter un objet à la BDD

    db.push(req.query);

    // 3 - Ecrire par dessus le fichier json
    

    // 4 - renvoyer un message de succès (ou d'erreur)
    
    res.send("ok");
});


app.get('/article/:mot', function(req, res){
  var mot = req.params.mot;
  console.log(mot);
  res.send("ok " + mot + " !");
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});