# Projet 

## développeurs :

- Mylène
- Élodie
- Maxime
- Dimitri

## Installation

```
git clone git@gitlab.com:maxime-thomas-dimitri-myl-ne/fil-rouge--glossaire.git
cd fil-rouge--glossaire
npm install
```

## Exécution 

```
node index.js
```